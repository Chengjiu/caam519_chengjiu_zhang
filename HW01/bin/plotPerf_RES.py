#! /usr/bin/env python
import os

sizes = []
times1 = []
times2 = []
for k in range(5):
  Nx = 10 * 2**k
  modname1 = 'perf_RES_ILU%d' % k
  modname2 = 'perf_RES_GAMG%d' % k
  options1 = ['-da_grid_x', str(Nx), '-da_grid_y', str(Nx), '-log_view', ':%s.py:ascii_info_detail' % modname1, '-ksp_type gmres -pc_type ilu']
  options2 = ['-da_grid_x', str(Nx), '-da_grid_y', str(Nx), '-log_view', ':%s.py:ascii_info_detail' % modname2, '-ksp_type gmres -pc_type gamg']
  os.system('./ex5 '+' '.join(options1))
  perfmod1 = __import__(modname1)
  os.system('./ex5 '+' '.join(options2))
  perfmod2 = __import__(modname2)
  sizes.append(Nx**2)
  #times.append(perfmod.Stages['Main Stage']['SNESSolve'][0]['time'])
  times1.append(perfmod1.Stages['Main Stage']['SNESFunctionEval'][0]['time'])
  times2.append(perfmod2.Stages['Main Stage']['SNESFunctionEval'][0]['time'])
print zip(sizes, times1,times2)

from pylab import legend, plot, loglog, show, title, xlabel, ylabel
plot(sizes,times1,label="GMRES/ILU")
plot(sizes,times2,label="GMRES/GAMG")
title('RES ex5')
xlabel('Problem Size $N$')
ylabel('Time (s)')
legend(loc="upper left")
show()

loglog(sizes, times1,label="GMRES/ILU")
loglog(sizes, times2,label="GMRES/GAMG")
title('RES ex5')
xlabel('Problem Size $N$')
legend(loc="upper left")
ylabel('Time (s)')
show()
