#include <petscdm.h>
#include <petscdmda.h>
#include <petscsnes.h>
#include <petscmatlab.h>

#include <petsc.h>
#include <petscvec.h>
#include <petscmat.h>

static char help[] = "testing QR decomposition\n";

int main(int argc,char **argv){
	
	Mat M,Q,R,QR,DIFF;
	Vec Mi,Qj,tMi,tQj;
	PetscInt dim = 4,i,j;
	PetscScalar *m,*mi,*q,*qj,temp,*v,c;
	PetscReal nor;
	
	
	PetscInitialize(&argc,&argv,(char*)0,help);
	
	MatCreate(PETSC_COMM_SELF,&M);
	MatSetSizes(M, dim, dim, dim, dim);
	MatSetType(M,MATSEQDENSE);
	MatSeqDenseSetPreallocation(M,NULL);
	
	MatCreate(PETSC_COMM_SELF,&Q);
	MatSetSizes(Q, dim, dim, dim, dim);
	MatSetType(Q,MATSEQDENSE);
	MatSeqDenseSetPreallocation(Q,NULL);
	
	MatCreate(PETSC_COMM_SELF,&R);
	MatSetSizes(R, dim, dim, dim, dim);
	MatSetType(R,MATSEQDENSE);
	MatSeqDenseSetPreallocation(R,NULL);
	
	MatCreate(PETSC_COMM_SELF,&QR);
	MatSetSizes(QR, dim, dim, dim, dim);
	MatSetType(QR,MATSEQDENSE);
	MatSeqDenseSetPreallocation(QR,NULL);

	MatCreate(PETSC_COMM_SELF,&DIFF);
	MatSetSizes(DIFF, dim, dim, dim, dim);
	MatSetType(DIFF,MATSEQDENSE);
	MatSeqDenseSetPreallocation(DIFF,NULL);


	//Set M
	for(i = 0; i < dim; i++){
		for(j = 0; j < dim; j++){
			c = rand()%100;
			MatSetValues(M,1,&i,1,&j,&c,INSERT_VALUES);
		}
	}
	
	//get array of matrix M
	MatDenseGetArray(M,&m);

	//Get Q
	MatAssemblyBegin(M,MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(M,MAT_FINAL_ASSEMBLY);	
	MatAssemblyBegin(Q,MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(Q,MAT_FINAL_ASSEMBLY);
	MatAssemblyBegin(R,MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(R,MAT_FINAL_ASSEMBLY);
	MatAssemblyBegin(QR,MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(QR,MAT_FINAL_ASSEMBLY);	
	MatAssemblyBegin(DIFF,MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(DIFF,MAT_FINAL_ASSEMBLY);
	
	for(i = 0; i < dim; i++){
		mi = &m[dim * i];
		VecCreateSeqWithArray(MPI_COMM_SELF,1,dim,mi,&tMi);	
		VecDuplicate(tMi,&Mi);
		VecCopy(tMi,Mi);
		MatDenseGetArray(Q,&q);
		for(j = 0; j < i; j++){
			qj = &q[dim * j];
			VecCreateSeqWithArray(MPI_COMM_SELF,1,dim,qj,&tQj);
			VecDuplicate(tQj,&Qj);
			VecCopy(tQj,Qj);
			VecTDot(Mi,Qj,&temp);
			VecScale(Qj, temp);
			VecAXPY(Mi,-1.0,Qj);
		}
		
		VecNorm(Mi,1,&nor);
		VecScale(Mi, 1.0 / nor);
		VecGetArray(Mi,&v);

		for(j=0;j<dim;j++)
		{
			q[j+i*dim] = v[j];
		}

		
	}
	//Get R
	MatTransposeMatMult(Q,M,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&R);
	//Check QR == M
	
	//Output
	//M	
	PetscPrintf(PETSC_COMM_WORLD,"Original Matrix M:\n");
	MatView(M,PETSC_VIEWER_STDOUT_WORLD);

	//Q
	PetscPrintf(PETSC_COMM_WORLD,"Q for QR Decomposition:\n");	
	MatView(Q,PETSC_VIEWER_STDOUT_WORLD);
	
	
	//R
	PetscPrintf(PETSC_COMM_WORLD,"R for QR Decomposition:\n");
	MatView(R,PETSC_VIEWER_STDOUT_WORLD);
	
	//QR
	MatMatMult(Q,R,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&QR);
	PetscPrintf(PETSC_COMM_WORLD,"Result of QR:\n");	
	MatView(QR,PETSC_VIEWER_STDOUT_WORLD);

	//DIFF
	MatDuplicate(QR,MAT_COPY_VALUES,&DIFF);
	MatAXPY(DIFF,-1.0,M,SAME_NONZERO_PATTERN);
	PetscPrintf(PETSC_COMM_WORLD,"Result of DIFF =  QR - M:\n");	
	MatView(DIFF,PETSC_VIEWER_STDOUT_WORLD);
	return 0;
}
