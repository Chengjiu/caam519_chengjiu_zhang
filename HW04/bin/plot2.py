import os
import numpy as np
from pylab import legend, plot,loglog,show,title,xlabel,ylabel

# lambda = 1.0 mms 1
err_gamg=np.array([4.8539e-11,2.49321e-11,2.61399e-10,1.31694e-10,6.61006e-11])
err_lu=np.array([4.85389e-11,2.49318e-11,2.61399e-10,1.31691e-10,6.6099e-11])
flops_gamg=np.array([15710500.0, 63650600.0, 219002000.0, 947249000.0, 4491410000.0]
)
flops_lu=np.array([4205510.0, 33938200.0, 229030000.0, 1845500000.0, 14822700000.0])


# lambda = 1.0 mms 2
#err_gamg=np.array([1.2437e-05,1.60219e-06,2.03367e-07,2.5442e-08,3.12598e-09])
#err_lu=np.array([1.2437e-05,1.60219e-06,2.03367e-07,2.5442e-08,3.12599e-09])
#flops_gamg=np.array([13094100.0, 52960700.0, 218602000.0, 756936000.0, 3591570000.0])
#flops_lu=np.array([3484260.0, 28203000.0, 228631000.0, 1475500000.0, 11854600000.0])



loglog(flops_gamg,err_gamg,'b-',flops_lu,err_lu,'r-')
title('Work-Precison Diagram for MMS1 with lambda = 1.0')
legend(['GAMG','LU'],'best')
xlabel('Flops')
ylabel('l_2 error')
show() 
