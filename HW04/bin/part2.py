import os

flops_gamg=[]
flops_lu=[]
for i in range(1,6):
    modname_lu='flop_lu%d'%i
    modname_gamg='flop_gamg%d'%i
    option_gamg = ['-pc_type gamg -pc_mg_levels 3 -pc_mg_galerkin -da_grid_x 17 -da_grid_y 17 -da_refine %d'%i,
                   ' -mg_levels_ksp_norm_type unpreconditioned -mg_levels_ksp_chebyshev_esteig 0.5,1.1',
                   ' -mg_levels_pc_type sor -pc_mg_type full -mms %d -par %f'%(1, 1.0),
                   '-log_view',':%s.py:ascii_info_detail'%modname_gamg] 
    os.system('./ex5 '+' '.join(option_gamg))
    mod_gamg=__import__(modname_gamg)
    flops_gamg.append(mod_gamg.LocalFlops[0])

    option_lu = ['-pc_type lu -da_grid_x 17 -da_grid_y 17 -da_refine %d'%i,
                 '-mms %d -par %f'%(1, 1.0),
                 '-log_view',':%s.py:ascii_info_detail'%modname_lu]     
    os.system('./ex5 '+' '.join(option_lu))
    mod_lu=__import__(modname_lu)
    flops_lu.append(mod_lu.LocalFlops[0])

print flops_gamg
print flops_lu
