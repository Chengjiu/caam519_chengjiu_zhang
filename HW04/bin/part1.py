import os

lambdas = [1.0,2.0,5.0]
mms = [1,2]

for amms in mms:
    print "mms is %d"%amms
    for alambda in lambdas:
        print "lambda is %d"%alambda
        for i in range(1,6):
            option = ['-pc_type gamg -pc_mg_levels 3 -pc_mg_galerkin -da_grid_x 17 -da_grid_y 17 -da_refine %d'%i,
                      ' -mg_levels_ksp_norm_type unpreconditioned -mg_levels_ksp_chebyshev_esteig 0.5,1.1',
                      ' -mg_levels_pc_type sor -pc_mg_type full -mms %d -par %f'%(amms, alambda)]
            os.system('./ex5 '+' '.join(option))


