import numpy as np

from pylab import legend,plot,loglog,show,title,xlabel,ylabel


N = np.array([1089,4225,16641,66049,263169])

# mms = 1, lambda = 1.0
el2=np.array([4.8539e-11,2.49321e-11,2.61399e-10,1.31694e-10,6.61006e-11])
einf=np.array([3.2901e-09,3.27837e-09,6.76827e-08,6.767e-08,6.76666e-08])

# mms = 1, lambda = 2.0
#el2=np.array([9.88362e-12,5.0801e-12,2.57788e-12,2.74536e-10,1.378e-10])
#einf=np.array([7.26603e-10,7.24308e-10,7.23773e-10,1.52613e-07,1.52604e-07])

# mms = 1, lambda = 5.0
#el2=np.array([2.19014e-11,1.23322e-11,6.50439e-12,3.26319e-12,1.63057e-12])
#einf=np.array([1.91202e-09,1.99552e-09,2.55739e-09,2.53142e-09,2.10768e-09])

# mms = 2, lambda = 1.0
#el2=np.array([1.2437e-05,1.60219e-06,2.03367e-07,2.5442e-08,3.12598e-09])
#einf=np.array([0.000846495,0.00021154,5.28788e-05,1.31482e-05,3.23343e-06])

# mms = 2, lambda = 2.0
#el2=np.array([1.27387e-05,1.64097e-06,2.07866e-07,2.60227e-08,3.18397e-09])
#einf=np.array([0.000872202,0.000217945,5.43638e-05,1.35025e-05,3.28751e-06])

# mms = 2, lambda = 5.0
#el2=np.array([1.3124e-05,1.69031e-06,2.14513e-07,2.6795e-08,3.27493e-09])
#einf=np.array([0.000915985,0.000228809,5.71806e-05,1.4145e-05,3.42148e-06])

loglog(N, el2, 'r', N, 0.1*N**-1.5, 'r--',N, einf, 'g', N, 0.5/N, 'g--')
title('Part I MMS 1 Lambda 1')
xlabel('Number of Dof N')
ylabel('Solution Error e')
legend(['l_2','h^{-3} = N^{-3/2}',
'l_infty', 'h^{-2} = N^{-1}'], 'best')
show()
